## Памятка по настройке отладки тестов через Xdebug в PhpStorm, запускаемых на удаленном сервере или PhpStorm + PhpUnit and Xdebug on Remote Host

- `PhpStorm -> Settings -> Languages & Frameworks -> PHP -> Debug`  
  - В секции Xdebug указываем `Debug port` = `9003`  
    (значение из `php -i | grep xdebug.remote_port` на удаленном сервере)

- `PhpStorm -> Settings -> Languages & Frameworks -> PHP -> Debug -> DBGp Proxy`  
  - В поле `IDE key` указываем `PHPSTORM`  
    (значение из `php -i | grep xdebug.idekey` на удаленном сервере)
  - В поле `Port` указываем `9003`  
    (значение из `php -i | grep xdebug.remote_port` на удаленном сервере)

- Некоторые значения из `php -i | grep xdebug` на удаленном сервере
```
  xdebug.idekey => PHPSTORM => PHPSTORM
  xdebug.remote_autostart => Off => Off
  xdebug.remote_connect_back => On => On
  xdebug.remote_enable => On => On
  xdebug.remote_handler => dbgp => dbgp
  xdebug.remote_host => localhost => localhost
  xdebug.remote_mode => req => req
  xdebug.remote_port => 9003 => 9003
```

- `PhpStorm -> Run (Top menu item) -> Start Listening for
PHP Debug Connections`

На текущем моменте мы имеем запущенный PhpStorm, который
слушает входящие соединения на 9003 порту на localhost.
PhpStorm будет принимать только те соединения, которые
передают IDE key PHPSTORM, остальные будет игнорировать.
Но тесты мы будем запускать на удаленном хосте. И Xdebug будет
посылать запрос на подключение на localhost:9003 на удаленном
сервере. Как сделать, чтобы PhpStorm мог принимать соединения с
удаленного сервера? Нужно просто сделать так, чтобы 9003 порт
на локальной машине пробросился на 9003 порт на удаленном сервере.
И когда Xdebug будет подключаться на localhost:9003 на удаленном
сервере он попадет на 9003 порт на нашей локальной машине, который
слушает PhpStorm. Пробросить порт можно так:
```bash
ssh -R 9003:localhost:9003 user@host
```
Чтобы Xdebug передал IDE key PHPSTORM при запуске из консоли на
удаленном сервере нужно объявить следующую переменную:
```bash
export XDEBUG_CONFIG="idekey=PHPSTORM"
```
Теперь можно ставить точку останова в PhpStorm и запускать
тесты на удаленном сервере.
```bash
php vendor/bin/phpunit tests
```
Когда отлаживать тесты больше не нужно, нужно удалить ранее
объявленную переменную, иначе PhpStorm будет останавливаться
на точках останова при любом запуске php скриптов из консоли
на удаленном сервере.
```bash
unset XDEBUG_CONFIG
```
